## Jacob Lebold
Bachelor of Science in Computer Science, No specialization

## Q/A
**What is your technical background?**

I am a 4th year computer science student at BGSU. Other than classwork experience, I was fortunate enough to work at Berkeley County Administrations IT department, working with a talented web development team as an intern. The experience greatly grew my understanding of web development, as well as the responsibilities that come with working as a team. 

**What most concerns you about this course?**

My biggest concern with this course is not the actual concepts that we will be learning, but being able to implement them into a working project. I find both areas to be super interesting, but still believe implmenting all the concepts we learn will be a fun challenge to overcome.

**What do you expect to get out of this course?**

I expect to get a deeper understanding of what AI is, as well as many different concepts that go into the creation of AI tools. As this course gets into Big Data analytics, I believe it will help me in growing my understanding and skills in looking at data, and using the knowledge attained from that data to better the program I am working on, both in class and in the future.

**Are there things that will help you succeed in this course?**

I believe this course will take a lot of work, especially since AI is a new topic for myself, and from the outside it seems like it is a massive undertaking to learn about. Putting in that extra effort outside of class will be huge in helping with this class, as well as studying outside resources to get a deeper understanding of what we are learning as well. 

**Is there anything else you would like the instructor to know?**

Not really, just that so far I have been enjoying the lectures and look forward to progressing through this class!

**In your opinion, what is AI? what can AI do? what do you want to do with AI?**

AI is a tool that can actually learn and get better overtime. AI's potential for what it can do it massive, as it can be used for simple things, like cleaning robots, online shopping, etc., and can be used for complex problem solving like helping in creating and verifying complex math equations and biological studies. As for what I want to do with AI, I think it would be cool to use AI for personal gain. Like i mentioned before, I believe creating an AI that is able to help me find the best deals when shopping online could be a very fun side project, and would look extremely good on a resume as well!
